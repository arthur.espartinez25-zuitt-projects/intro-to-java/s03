package com.espartinez.s03activity;

import java.util.Scanner;

public class ComputeFactorial {

    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        System.out.println("Input an integer number: ");
        var answer =1;
        var initial=1;
        try{
            var num = in.nextInt();
            if(num >= 0){
                if(num > 0){
                    while(initial<=num){
                        answer=answer*initial;
                        initial++;
                    }
                }
                System.out.println("The factorial of "+num+" is "+answer);
            } else {
                System.out.println("Invalid input!");
            }
        } catch(Exception err){
            System.out.println("Invalid input!");
        }

    }
}
